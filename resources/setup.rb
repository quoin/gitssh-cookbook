actions :create

attribute :user, :kind_of => String, :name_attribute => true
attribute :group, :kind_of => String, :default => nil
attribute :private_key, :kind_of => String
attribute :ssh_directory, :kind_of => String, :default => nil

def initialize(*args)
  super
  @action = :create
end
