name             'git_ssh'
maintainer       'Ben Keith'
maintainer_email 'ben.keith@quoininc.com'
license          'All rights reserved'
description      'Installs/Configures ssh for use by git to securely grab files from a remote repository.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
