git\_ssh Cookbook
================
Installs ssh files, including a keypair, a known hosts file, and a git-wrapper
script to securely connect to a remote repository.

This cookbook facilitates securely cloning repositories from github/bitbucket
using ssh deploy keys.  It forces known host key checking by ssh to ensure you
are talking to the right repository service.  It includes known host entries
for github and bitbucket.


Requirements
------------
None


Usage
-----
You need to include the default recipe after the user who is going to own the
keys has been created, but before the git repository is cloned.

Example loading the deploy key from Chef Vault:

```ruby
deploy_key = ChefVault::Item.load('deploy_key', node.chef_environment)['private_key']
git_ssh_setup node[:myapp][:app_user] do
  group node[:myapp][:app_group]
  private_key deploy_key
  ssh_directory ::File.join(node[:myapp][:app_home], '.ssh')
end
```

It doesn't matter where the key comes from but it should be a string with the
key.  Change the other resource attributes as appropriate.

After you have run that resource, you should be able to use the `git` resource
like normal with the same user, provided that the provided deploy key has
access to the desired repo.
