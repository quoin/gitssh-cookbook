
action :create do
  ssh_dir = new_resource.ssh_directory || ::File.join(::Dir.home(new_resource.user), '.ssh')
  directory ssh_dir do
    owner new_resource.user
    group new_resource.group
    mode '0700'
  end

  private_key_name = 'id_rsa-git'
  file ::File.join(ssh_dir, private_key_name) do
    mode '0400'
    owner new_resource.user
    group new_resource.group
    content new_resource.private_key
  end

  cookbook_file ::File.join(ssh_dir, 'known_hosts') do
    source "known_hosts"
    owner new_resource.user
    group new_resource.group
    cookbook 'git_ssh'
  end

  git_wrapper_path = ::File.join(ssh_dir, 'git-wrapper.sh')
  template git_wrapper_path do
    source 'ssh_wrapper.sh.erb'
    mode '0744'
    owner new_resource.user
    group new_resource.group
    cookbook 'git_ssh'
    variables({
      :deploy_private_key_path => ::File.join(ssh_dir, private_key_name),
      :known_hosts_file => ::File.join(ssh_dir, 'known_hosts'),
    })
  end

  ENV['GIT_SSH'] = git_wrapper_path
end
